#!/usr/bin/env bash

# Create user
useradd -m $USER_NAME -p $USER_PWD_HASH -s /bin/bash
sed -i  -e "s/#AllowUsers/AllowUsers $USER_NAME/" /etc/ssh/sshd_config

# start SSH daemon
/usr/sbin/sshd -D
